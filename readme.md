# jambp

JAMstack full-stack web application boilerplate

## Install

```bash
npx degit gitlab:seangenabe/jambp
```

## Stack

Only the best:
* **GatsbyJS** for frontend React development
* **Tailwind CSS + PurgeCSS** for atomic CSS
* **Material-UI** components (swappable)
* **Fastify** for a lightning-speed backend server, faster than Express

### Development

`npm run watch` will:
* Run the Gatsby development server with `nodemon` and `ts-node`.
  * Reloads when `.ts` files on the root directory changes (via `nodemon`)
  * Reloads when files on the `src` directory changes (via `gatsby-cli`)
* Runs the backend server.

Note: You should mind Gatsby's server instead of the backend server (`http://localhost:8000`). The backend server is [proxied](https://www.gatsbyjs.org/docs/api-proxy/) through the Gatsby server.

### Production

`npm run build` will:
* Build the static files (via `gatsby-cli`)

`npm start` will:
* Start the backend server.

This time you should mind the backend server (`http://localhost:80`) as it will be the only server running.
