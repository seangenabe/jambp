import Fastify, { ServerOptions } from "fastify"
import FastifyHelmet from "fastify-helmet"
import FastifyStatic from "fastify-static"
import Arecibo from "arecibo"

export function createServer(opts: ServerOptions = {}) {
  const server = Fastify({ logger: { level: "info" }, ...opts })

  server.register(FastifyHelmet)
  server.register(Arecibo, {
    readinessURL: "/readiness",
    livenessURL: "/liveness"
  })

  server.register(FastifyStatic, {
    root: `${__dirname}/../public`,
    prefix: "/"
  })

  server.register(
    (server, _, next) => {
      server.route({
        method: "GET",
        url: "/hello",
        schema: {
          response: {
            200: {
              type: "string"
            }
          }
        },
        async handler() {
          // intentional delay for demo
          await new Promise(r => setTimeout(r, 3000))
          return "Hello world"
        }
      })
      next()
    },
    { prefix: "/-" }
  )

  return server
}

async function start() {
  const server = createServer()
  try {
    const port = Number(process.env.PORT) || 80
    await server.listen(port)
  } catch (err) {
    server.log.error(err)
    process.exit(1)
  }
}

if (require.main === module) {
  start()
}
