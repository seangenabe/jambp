import "source-map-support/register"
import PurgeCssPlugin from "purgecss-webpack-plugin"
import purgeCssConfig from "./purgecss.config"

export function onCreateWebpackConfig({
  actions,
  stage
}: {
  actions: any
  stage: string[]
}) {
  if (stage.includes("develop")) {
    actions.setWebpackConfig({
      devtool: "inline-source-map"
    })
    return
  }

  // Add PurgeCSS in production
  // See: https://github.com/gatsbyjs/gatsby/issues/5778#issuecomment-402481270
  if (stage.includes("build")) {
    actions.setWebpackConfig({
      plugins: [new PurgeCssPlugin(purgeCssConfig)]
    })
  }
}
