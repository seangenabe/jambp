import tailwind from "tailwindcss"
import precss from "precss"
import tailwindConfig from "./tailwind.config"

export const plugins = [precss(), tailwind(tailwindConfig)]
