import { plugins as postCssPlugins } from "./postcss.config"
import proxy from "http-proxy-middleware"

export const siteMetadata = {
  title: "JAMBP",
  description: "An awesome website built with JAMstack",
  shortTitle: "JAMBP",
  author: "John Smith",
  keywords: ["website" as const, "demo" as const],
  themeColor: "#a0a0ff"
} as const

export const plugins = [
  "gatsby-plugin-typescript",
  "gatsby-plugin-react-helmet",
  {
    resolve: "gatsby-source-filesystem",
    options: {
      name: "src",
      path: `${__dirname}/src`
    }
  },
  {
    resolve: "gatsby-plugin-manifest",
    options: {
      name: siteMetadata.title,
      short_name: siteMetadata.shortTitle,
      start_url: "/",
      background_color: siteMetadata.themeColor,
      theme_color: siteMetadata.themeColor,
      display: "minimal-ui",
      icon: "static/icon.png"
    }
  },
  {
    resolve: "gatsby-plugin-postcss",
    options: {
      postCssPlugins
    }
  },
  "gatsby-plugin-catch-links",
  {
    resolve: "gatsby-plugin-nprogress",
    options: {
      color: siteMetadata.themeColor,
      showSpinner: false
    }
  },
  "gatsby-plugin-offline"
  //"gatsby-plugin-netlify-cache",
  //"gatsby-plugin-netlify" // make sure to put last in the array
]

export function developMiddleware(app) {
  app.use(proxy("http://localhost:80/-/**"))
}
