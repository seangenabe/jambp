import React from "react"
import { ReactNode } from "react"
import { MainAppBar } from "./main-app-bar"
import "../styles/styles.css"
import CssBaseline from "@material-ui/core/CssBaseline"
import purple from "@material-ui/core/colors/purple"
import { Provider as ReduxProvider } from "react-redux"
import { store } from "./store/store"
import { MainDrawer } from "./main-drawer"
import { MetaProps, Meta } from "./meta"
import { Helmet } from "react-helmet"
import {
  createStyles,
  Theme,
  withStyles,
  WithStyles,
  MuiThemeProvider,
  createMuiTheme
} from "@material-ui/core/styles"

const theme = createMuiTheme({
  palette: {
    primary: purple
  },
  typography: {
    useNextVariants: true
  }
})

function renderLayout(props: LayoutProps & WithStyles<typeof styles>) {
  const { children, classes } = props
  return (
    <ReduxProvider store={store}>
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <Helmet>
          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500"
          />
          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/icon?family=Material+Icons"
          />
          <body className="leading-loose font-sans" />
        </Helmet>
        <Meta {...props} />
        <div className={classes.root}>
          <MainAppBar />
          <MainDrawer />
          <div className={classes.content}>
            <div className={classes.toolbar} />
            <main>{children}</main>
          </div>
        </div>
      </MuiThemeProvider>
    </ReduxProvider>
  )
}

export const Layout = withStyles(styles)(renderLayout)

function styles(theme: Theme) {
  return createStyles({
    toolbar: theme.mixins.toolbar,
    content: {
      flexGrow: 1,
      padding: theme.spacing.unit * 3
    },
    root: {
      display: "flex"
    }
  })
}

export interface LayoutProps extends MetaProps {
  children?: ReactNode
}
