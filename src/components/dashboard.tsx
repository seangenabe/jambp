import React, { Component, ReactNode } from "react"
import { connect } from "react-redux"
import { State, fetchServerStatus } from "./store/store"

class DashboardImpl extends Component<StateProps & DispatchProps> {
  render() {
    const { serverStatus } = this.props
    let ssSpan: ReactNode
    if (serverStatus.isLoading) {
      ssSpan = <span className="text-grey">Loading...</span>
    } else if (serverStatus.error) {
      ssSpan = <span className="text-red">Error occurred.</span>
    } else {
      ssSpan = <span className="text-blue">{serverStatus.value}</span>
    }
    return <div>Server status: {ssSpan}</div>
  }

  componentDidMount() {
    this.props.fetchServerStatus()
  }
}

export const Dashboard = connect<StateProps, DispatchProps, {}, State>(
  ({ serverStatus }) => ({ serverStatus }),
  dispatch => ({
    fetchServerStatus: () => dispatch(fetchServerStatus())
  })
)(DashboardImpl)

interface StateProps {
  serverStatus: State["serverStatus"]
}

interface DispatchProps {
  fetchServerStatus(): void
}
