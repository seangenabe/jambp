import { graphql, StaticQuery } from "gatsby"
import React from "react"
import { Helmet } from "react-helmet"

export function Meta(args: MetaProps) {
  return (
    <StaticQuery
      query={graphql`
        {
          site {
            siteMetadata {
              title
              description
              shortTitle
              author
              keywords
            }
          }
        }
      `}
      render={data => {
        const meta = data.site.siteMetadata
        const title: string = args.title || meta.title
        const description: string = args.description || meta.description
        return (
          <Helmet
            htmlAttributes={{ lang: "en" }}
            titleTemplate={"%s - ${meta.title}"}
            title={args.title}
            defaultTitle={meta.title}
            meta={[
              { name: "description", content: description },
              { property: "og:title", content: title },
              { property: "og:type", content: "website" },
              { property: "twitter:card", content: "summary" },
              { name: "twitter.creator", content: meta.author },
              { name: "twitter.title", content: title },
              { name: "twitter:description", content: description },
              {
                name: "keywords",
                content: distinct([...(args.keywords || []), ...meta.keywords])
              },
              ...(args.meta || [])
            ]}
          />
        )
      }}
    />
  )
}

function distinct<T>(iter: Iterable<T>): T[] {
  return Array.from(new Set(iter))
}

export interface MetaProps {
  title?: string
  description?: string
  keywords?: readonly string[]
  meta?: readonly Readonly<{ property: string; content: string }>[]
}
