import React, { Component } from "react"
import { Link } from "gatsby"
import ListItem from "@material-ui/core/ListItem"
import ListItemIcon, { ListItemIconProps } from "@material-ui/core/ListItemIcon"
import ListItemText, { ListItemTextProps } from "@material-ui/core/ListItemText"
import {
  Theme,
  createStyles,
  withStyles,
  WithStyles
} from "@material-ui/core/styles"

class ListItemLinkImpl extends Component<
  ListItemLinkProps & WithStyles<typeof styles>
> {
  renderLink = myProps => (
    <Link
      to={this.props.to}
      activeClassName={this.props.classes.selected}
      {...myProps}
    />
  )

  render() {
    const { icon, primary } = this.props
    return (
      <ListItem button component={this.renderLink}>
        {icon && <ListItemIcon>{icon}</ListItemIcon>}
        <ListItemText primary={primary} />
      </ListItem>
    )
  }
}

export const ListItemLink = withStyles(styles)(ListItemLinkImpl)

function styles(theme: Theme) {
  return createStyles({
    selected: {
      backgroundColor: theme.palette.action.selected
    }
  })
}

export interface ListItemLinkProps {
  to: string
  icon: ListItemIconProps["children"]
  primary: ListItemTextProps["primary"]
}
