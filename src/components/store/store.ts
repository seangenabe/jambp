import { createStore, combineReducers, applyMiddleware } from "redux"
import { middleware as ReduxPack, handle } from "redux-pack"
import ky from "ky"

export const TOGGLE_IS_DRAWER_OPEN = "TOGGLE_IS_DRAWER_OPEN" as const
export const FETCH_SERVER_STATUS = "FETCH_SERVER_STATUS" as const

export function toggleIsDrawerOpen(): ToggleIsDrawerOpenAction {
  return { type: TOGGLE_IS_DRAWER_OPEN }
}

export function fetchServerStatus() {
  return {
    type: FETCH_SERVER_STATUS,
    promise: ky("/-/hello").text()
  }
}

const rootReducer = combineReducers({
  isDrawerOpen: (state: State["isDrawerOpen"] = false, action: ActionTypes) => {
    if (action.type === TOGGLE_IS_DRAWER_OPEN) {
      return !state
    }
    return state
  },
  serverStatus: (
    state: State["serverStatus"] = { value: "", error: "", isLoading: false },
    action: ActionTypes
  ) => {
    if (action.type === FETCH_SERVER_STATUS) {
      const act2 = action as any
      return handle(state, action, {
        start: state => ({ ...state, isLoading: true, error: "" }),
        finish: state => ({ ...state, isLoading: false }),
        failure: state => ({ ...state, error: act2.payload }),
        success: state => ({ ...state, value: act2.payload })
      })
    }
    return state
  }
})

export const store = createStore(rootReducer, applyMiddleware(ReduxPack))

export interface State {
  readonly isDrawerOpen: boolean
  readonly serverStatus: {
    value: string
    isLoading: boolean
    error: string
  }
}

export interface ToggleIsDrawerOpenAction {
  type: typeof TOGGLE_IS_DRAWER_OPEN
}

export interface SetServerStatusAction {
  type: typeof FETCH_SERVER_STATUS
  promise: ReturnType<typeof ky>
}

export type ActionTypes = ToggleIsDrawerOpenAction | SetServerStatusAction
