import React, { Fragment } from "react"
import {
  withStyles,
  WithStyles,
  createStyles,
  Theme
} from "@material-ui/core/styles"
import Drawer from "@material-ui/core/Drawer"
import List from "@material-ui/core/List"
import DashboardIcon from "@material-ui/icons/Dashboard"
import StarIcon from "@material-ui/icons/Star"
import Hidden from "@material-ui/core/Hidden"
import { State, toggleIsDrawerOpen } from "./store/store"
import { connect } from "react-redux"
import { CSSProperties } from "@material-ui/core/styles/withStyles"
import { ListItemLink } from "./list-item-link"

export const DRAWER_WIDTH = 240

function renderMainDrawer(
  props: MainDrawerProps &
    WithStyles<typeof styles> &
    StateProps &
    DispatchProps
) {
  const { classes, isDrawerOpen, toggleDrawer } = props

  const drawerContent = (
    <Fragment>
      <div className={classes.toolbar} />
      <List>
        <ListItemLink to="/" icon={<DashboardIcon />} primary="Dashboard" />
        <ListItemLink to="/second" icon={<StarIcon />} primary="Second Page" />
      </List>
    </Fragment>
  )
  return (
    <nav className={classes.drawer}>
      <Hidden smUp implementation="js">
        <Drawer
          variant="temporary"
          open={isDrawerOpen}
          onClose={() => toggleDrawer()}
          classes={{
            paper: classes.drawerPaper
          }}
        >
          {drawerContent}
        </Drawer>
      </Hidden>
      <Hidden xsDown implementation="css">
        <Drawer
          classes={{ paper: classes.drawerPaper }}
          variant="permanent"
          open
        >
          {drawerContent}
        </Drawer>
      </Hidden>
    </nav>
  )
}

function styles(theme: Theme) {
  return createStyles({
    hide: {
      display: "none"
    },
    drawer: {
      [theme.breakpoints.up("sm")]: {
        width: DRAWER_WIDTH,
        flexShrink: 0
      }
    },
    drawerPaper: {
      width: DRAWER_WIDTH
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing.unit * 3,
      transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      }),
      marginLeft: -DRAWER_WIDTH
    } as CSSProperties,
    contentShift: {
      transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen
      }),
      marginLeft: 0
    },
    toolbar: theme.mixins.toolbar
  })
}

export const MainDrawer = connect<
  StateProps,
  DispatchProps,
  MainDrawerProps,
  State
>(
  ({ isDrawerOpen }: State) => ({
    isDrawerOpen
  }),

  dispatch => ({
    toggleDrawer: () => dispatch(toggleIsDrawerOpen())
  })
)(withStyles(styles, { withTheme: true })(renderMainDrawer))




export interface MainDrawerProps {
  theme?: Theme
}

interface StateProps {
  isDrawerOpen: boolean
}

interface DispatchProps {
  toggleDrawer(): void
}
