import * as React from "react"
import { Layout } from "../components/layout"

export default function IndexPage() {
  return <Layout>The second page!</Layout>
}
